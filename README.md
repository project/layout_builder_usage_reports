# Layout Builder Usage Reports


## Introduction

Layout Builder Usage Reports intends to improve the default Drupal Layout
Builder management, providing a fast and full access to all blocks & paragraph
components being used in Layout builder.

 - For a full description of the module, visit the
   [project page](https://www.drupal.org/project/layout_builder_usage_reports)

 - To submit bug reports and feature suggestions, or to track changes visit the
   [issue queue](https://www.drupal.org/project/issues/search/layout_builder_usage_reports)


## Requirements

No special requirements.


## Features

Provides a new administrative report that:
 - Lists all nodes using layout builder,
 - Blocks & Block types being used in the layout,
 - Paragraph components & Paragraph types being used in the layout,
 - Options to filter by node bundle, language, block type, paragraph type.


## Installation

Install as you would normally install a contributed Drupal module.
See: [Installing modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
for further information.


## Configuration

1. Enable the module at Administration > Extend.
2. View the report at Administration > Reports > Layout Builder Usage Report
   (/admin/reports/layout/usage).

No additional configuration is required.


## Maintainers

 - Nikhil Dubbaka [TechNikh](https://www.drupal.org/u/technikh)
 - Michael Caldwell [justcaldwell](https://www.drupal.org/u/justcaldwell)
