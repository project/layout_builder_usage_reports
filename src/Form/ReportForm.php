<?php

namespace Drupal\layout_builder_usage_reports\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Constructs the report form.
 */
class ReportForm extends FormBase {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'report_form';
  }

  /**
   * Constructs a new ReportForm object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   */
  public function __construct(StateInterface $state, Connection $database) {
    $this->state = $state;
    $this->database = $database;
  }

  /**
   * Implements the create() method for the form.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   *   The instance of the form.
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('state'),
    $container->get('database')
    );
  }

  /**
   * Return true if a string starts with the specified character/ string.
   */
  private function startsWith($haystack, $needle) {
    $length = strlen($needle);
    return substr($haystack, 0, $length) === $needle;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $lbur_bundle = $this->state->get('lbur_bundle');
    $lbur_provider = $this->state->get('lbur_provider');
    $lbur_language = $this->state->get('lbur_language');
    $lbur_block_type = $this->state->get('lbur_block_type');
    $lbur_paragraph_type = $this->state->get('lbur_paragraph_type');

    $header = [
      'entity_id' => $this->t('Node ID'),
      'entity_title' => $this->t('Node Title'),
      'bundle' => $this->t('Bundle'),
      'langcode' => $this->t('Language'),
      'component_id' => $this->t('Plugin ID'),
      'label' => $this->t('Label'),
      'provider' => $this->t('Provider'),
    ];
    $database = $this->database;
    $result = [];
    $options = [];
    $display_reset = TRUE;
    if ($database->schema()->tableExists('node__layout_builder__layout')) {
      $query = $database->select('node__layout_builder__layout', 'nlb');
      $query->join('node_field_data', 'nfd', 'nlb.entity_id = nfd.nid AND nlb.langcode = nfd.langcode');
      $query->fields('nlb', [
        'bundle',
        'entity_id',
        'langcode',
        'layout_builder__layout_section',
      ]);
      $query->fields('nfd', ['title']);
      // If no filters and displaying all results, limit query.
      if (empty($lbur_bundle) && empty($lbur_provider) && empty($lbur_language) && empty($lbur_block_type) && empty($lbur_paragraph_type)) {
        $query->range(0, 500);
        $display_reset = FALSE;
      }
      $result = $query->execute();
    }
    $bundles_set = [];
    $providers_set = [];
    $languages_set = [];
    $block_types_set = [];
    $paragraph_types_set = [];
    foreach ($result as $record) {
      $node_title = $record->title;
      if (strlen($node_title) > 25) {
        $node_title = substr($node_title, 0, 23) . '..';
      }
      $node_link = Link::createFromRoute($node_title, 'entity.node.canonical', ['node' => $record->entity_id]);
      $bundles_set[$record->bundle] = $record->bundle;
      $languages_set[$record->langcode] = $record->langcode;

      if (!empty($lbur_bundle) && $lbur_bundle != $record->bundle) {
        continue;
      }
      if (!empty($lbur_language) && $lbur_language != $record->langcode) {
        continue;
      }
      $serialized_data = $record->layout_builder__layout_section;
      $unserialized_data = unserialize($serialized_data,
        [
          'allowed_classes' => [
            'Drupal\layout_builder\Section',
            'Drupal\layout_builder\SectionComponent',
            'Drupal\Component\Render\FormattableMarkup',
            'Drupal\Core\StringTranslation\TranslatableMarkup',
          ],
        ]
      );

      $components = $unserialized_data->getComponents();
      foreach ($components as $component) {
        $pluginId = $component->getPluginId();
        /*
         * inline_block:title_and_supporting_message
         * component:testimonial:node
         * if pluginId starts with inline_block:
         *   get second part of the ID after :, It's block type
         * if pluginId starts with component
         *   get second part of the ID after :, It's paragraph type
         */
        $pluginIdPartsArray = explode(":", $pluginId);
        $block_type = '';
        $paragraph_type = '';
        if ($this->startsWith($pluginId, "inline_block:")) {
          // It's block type.
          $block_type = ($pluginIdPartsArray[1] ?? '');
          if (!empty($block_type)) {
            $block_types_set[$block_type] = $block_type;
          }
        }
        elseif ($this->startsWith($pluginId, "component:")) {
          // It's paragraph type.
          $paragraph_type = ($pluginIdPartsArray[1] ?? '');
          if (!empty($paragraph_type)) {
            $paragraph_types_set[$paragraph_type] = $paragraph_type;
          }
        }
        $configuration = $component->get("configuration");
        $label = (isset($configuration["label"]) && !empty($configuration["label"]) ? $configuration["label"] : '');
        $provider = (isset($configuration["provider"]) && !empty($configuration["provider"]) ? $configuration["provider"] : 'No Provider');
        $providers_set[$provider] = $provider;
        if (!empty($lbur_provider) && $lbur_provider != $provider) {
          continue;
        }
        if (!empty($lbur_block_type) && $lbur_block_type != $block_type) {
          continue;
        }
        if (!empty($lbur_paragraph_type) && $lbur_paragraph_type != $paragraph_type) {
          continue;
        }
        $label_machine_name = preg_replace("/[^A-Za-z0-9]/", '', $label);
        $table_row_id = $record->entity_id . "-" . $record->langcode . "-" . $pluginId . "-" . $label_machine_name;
        if (strlen($label) > 25) {
          $label = substr($label, 0, 23) . '..';
        }
        $options[$table_row_id] = [
          'entity_id' => $record->entity_id,
          'entity_title' => $node_link,
          'bundle' => $record->bundle,
          'langcode' => $record->langcode,
          'component_id' => $pluginId,
          'label' => $label,
          'provider' => $provider,
        ];
      }
    }
    $form['filtergroup'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filter'),
      '#attributes' => ['class' => ['container-inline']],
    ];
    if (!empty($options)) {
      $form['filtergroup']['bundle'] = [
        '#empty_value' => '',
        '#type' => 'select',
        '#title' => $this->t('Node Bundle'),
        '#options' => $bundles_set,
        '#default_value' => $lbur_bundle,
        '#weight' => '0',
      ];
      $form['filtergroup']['provider'] = [
        '#empty_value' => '',
        '#type' => 'select',
        '#title' => $this->t('Provider'),
        '#options' => $providers_set,
        '#default_value' => $lbur_provider,
        '#weight' => '0',
      ];
      $form['filtergroup']['language'] = [
        '#empty_value' => '',
        '#type' => 'select',
        '#title' => $this->t('Node language'),
        '#options' => $languages_set,
        '#default_value' => $lbur_language,
        '#weight' => '0',
      ];
      $form['filtergroup']['block_type'] = [
        '#empty_value' => '',
        '#type' => 'select',
        '#title' => $this->t('Block Type'),
        '#options' => $block_types_set,
        '#default_value' => $lbur_block_type,
        '#weight' => '0',
      ];
      $form['filtergroup']['paragraph_type'] = [
        '#empty_value' => '',
        '#type' => 'select',
        '#title' => $this->t('Paragraph Type'),
        '#options' => $paragraph_types_set,
        '#default_value' => $lbur_paragraph_type,
        '#weight' => '0',
      ];
      $form['filtergroup']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Filter'),
        '#weight' => '8',
      ];
    }
    if ($display_reset) {
      $form['filtergroup']['reset'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset the filter'),
        '#submit' => [[$this, 'resetForm']],
      ];
    }
    if (!empty($options)) {
      $form['tablehelptext'] = [
        '#markup' => $this->formatPlural(count($options), '1 result shown below!', '@count results shown below!'),
        '#weight' => '9',
      ];
    }

    $form['report'] = [
      '#type' => 'table',
      '#title' => $this->t('Report'),
      '#description' => $this->formatPlural(count($options), '1 result shown below!', '@count results shown below!'),
      '#header' => $header,
      '#rows' => $options,
      '#empty' => $this->t('No layout data found'),
      '#weight' => '9',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @todo Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->state->set('lbur_bundle', $form_state->getValues()["bundle"]);
    $this->state->set('lbur_provider', $form_state->getValues()["provider"]);
    $this->state->set('lbur_language', $form_state->getValues()["language"]);
    $this->state->set('lbur_block_type', $form_state->getValues()["block_type"]);
    $this->state->set('lbur_paragraph_type', $form_state->getValues()["paragraph_type"]);
  }

  /**
   * Reset the filter.
   */
  public function resetForm() {
    $this->state->delete('lbur_bundle');
    $this->state->delete('lbur_provider');
    $this->state->delete('lbur_language');
    $this->state->delete('lbur_block_type');
    $this->state->delete('lbur_paragraph_type');
  }

}
